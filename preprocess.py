import json
from collections import Counter
from pathlib import Path
from typing import List, Tuple
import csv
import matplotlib.pyplot as plt
import pandas
import xlrd
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from neupy.algorithms import PNN
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import accuracy_score, hamming_loss, confusion_matrix
from sklearn.model_selection import learning_curve, KFold
from sklearn.utils.multiclass import unique_labels
import numpy as np

k = 5

labels = {
    '1': 'positif',
    '0': 'netral',
    '-1': 'negatif'
}


def get_data(b):
    d: List[Tuple[str, str]] = []
    for row in b:
        if len(row) < 1:
            continue
        if not row[0] or row[0] == " ":
            continue
        d.append((row[0], "" if len(row) < 2 else row[1]))

    return d


def clean_data(d: List[Tuple[str, str]]):
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    l: List[str] = []
    t: List[str] = []
    for idx, v in enumerate(d):
        temp = stemmer.stem(v[0].replace('\n', "").replace("\"", "").lower())
        print(temp)

        if str(v[1]) is '0':
            continue

        l.append(labels[str(v[1])])
        t.append(temp)

    return t, l


def plot_confusion_matrix(cm,
                          target_names,
                          title='Confusion matrix',
                          cmap=None,
                          normalize=True):
    """
    given a sklearn confusion matrix (cm), make a nice plot

    Arguments
    ---------
    cm:           confusion matrix from sklearn.metrics.confusion_matrix

    target_names: given classification classes such as [0, 1, 2]
                  the class names, for example: ['high', 'medium', 'low']

    title:        the text to display at the top of the matrix

    cmap:         the gradient of the values displayed from matplotlib.pyplot.cm
                  see http://matplotlib.org/examples/color/colormaps_reference.html
                  plt.get_cmap('jet') or plt.cm.Blues

    normalize:    If False, plot the raw numbers
                  If True, plot the proportions

    Usage
    -----
    plot_confusion_matrix(cm           = cm,                  # confusion matrix created by
                                                              # sklearn.metrics.confusion_matrix
                          normalize    = True,                # show proportions
                          target_names = y_labels_vals,       # list of names of the classes
                          title        = best_estimator_name) # title of graph

    Citiation
    ---------
    http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    """
    import matplotlib.pyplot as plt
    import numpy as np
    import itertools

    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(
        accuracy, misclass))
    plt.show()


def plot_learning_curve(estimator, title, X, y, ylim=None, cv=None,
                        n_jobs=1, train_sizes=pandas.np.linspace(.1, 1.0, 5)):
    """
       Generate a simple plot of the test and traning learning curve.

       Parameters
       ----------
       estimator : object type that implements the "fit" and "predict" methods
           An object of that type which is cloned for each validation.

       title : string
           Title for the chart.

       X : array-like, shape (n_samples, n_features)
           Training vector, where n_samples is the number of samples and
           n_features is the number of features.

       y : array-like, shape (n_samples) or (n_samples, n_features), optional
           Target relative to X for classification or regression;
           None for unsupervised learning.

       ylim : tuple, shape (ymin, ymax), optional
           Defines minimum and maximum yvalues plotted.

       cv : integer, cross-validation generator, optional
           If an integer is passed, it is the number of folds (defaults to 3).
           Specific cross-validation objects can be passed, see
           sklearn.cross_validation module for the list of possible objects

       n_jobs : integer, optional
           Number of jobs to run in parallel (default 1).
           :param train_sizes:
       """
    plt.figure()
    plt.title(title)
    if ylim is not None:
        plt.ylim(*ylim)
    plt.xlabel("Training examples")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes, scoring='accuracy')
    train_scores_mean = pandas.np.mean(train_scores, axis=1)
    train_scores_std = pandas.np.std(train_scores, axis=1)
    test_scores_mean = pandas.np.mean(test_scores, axis=1)
    test_scores_std = pandas.np.std(test_scores, axis=1)
    plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Cross-validation score")

    plt.legend(loc="best")
    return plt


def display_scores(vectorizer, tfidf_result):
    # http://stackoverflow.com/questions/16078015/
    scores = zip(vectorizer.get_feature_names(),
                 pandas.np.asarray(tfidf_result.sum(axis=0)).ravel())
    sorted_scores = sorted(scores, key=lambda x: x[1], reverse=True)
    for item in sorted_scores:
        print("{0:50} Score: {1}".format(item[0], item[1]))


def translate_label(label: str):
    return {
        'positif': 2,
        'negatif': 0,
        'netral': 1
    }[label]


if __name__ == "__main__":
    dataset_file = Path('./data_frame.json')
    datatest_file = Path('./data_test.json')
    stop_words = []
    with open('stopwords-id.txt') as f:
        for line in f:
            val = line.lower().split()
            stop_words.append(val[0])

    data_frame = pandas.DataFrame()
    test_data_frame = pandas.DataFrame()

    if not dataset_file.is_file():
        test_file = open('data-training.csv')
        csv_reader = csv.reader(test_file, delimiter=',')
        data = get_data(csv_reader)
        data_frame['texts'], data_frame['labels'] = clean_data(data)
        data_frame.to_json('data_frame.json')
        data_frame.to_csv('hasil_preprocess_training.csv')
    else:
        with open('./data_frame.json') as dataset_file:
            json_data = json.load(dataset_file)
            data_frame = data_frame.from_records(json_data)
            data_frame.to_csv('hasil_preprocess_training.csv')

    if not datatest_file.is_file():
        test_file = open('data-test.csv')
        csv_reader = csv.reader(test_file, delimiter=',')
        data = get_data(csv_reader)
        test_data_frame['texts'], test_data_frame['labels'] = clean_data(data)
        test_data_frame.to_json('data_test.json')
        test_data_frame.to_csv('hasil_preprocess_test.csv')

    else:
        with open('./data_test.json') as dataset_file:
            json_data = json.load(dataset_file)
            test_data_frame = data_frame.from_records(json_data)
            test_data_frame.to_csv('hasil_preprocess_test.csv')

    tfidf_vect = TfidfVectorizer(analyzer='word', min_df=0.1, stop_words=stop_words,
                                 token_pattern='(?u)\\b[a-zA-Z]\\w{2,}\\b', norm='l2')

    kf = KFold(n_splits=5)

    X_train = []
    Y_train = []
    result = next(kf.split(X=data_frame), None)
    tfidf_vect.fit(data_frame.iloc[result[0]]['texts'])
    xtrain_tfidf = tfidf_vect.transform(data_frame.iloc[result[0]]['texts'])
    print("dimension {}".format(xtrain_tfidf.shape))
    # print("frekuensi kata")
    # print("=============================")
    # print(data_frame.iloc[result[0]]['texts'])
    for idx, row in enumerate(data_frame.iloc[result[0]]['texts']):
        # print("text ke %d" % idx)
        splitted = row.split(' ')
        counts = Counter(splitted)
        # print(counts)

    # pandas.set_option('display.height', 1000)
    # pandas.set_option('display.max_rows', 500)
    # pandas.set_option('display.max_columns', 500)
    # pandas.set_option('display.width', 1000)
    # corpus_idx = [n for n in data_frame.iloc[result[0]]['texts'].keys()].sort()
    # df = pandas.DataFrame(xtrain_tfidf.T.todense(
    # ), index=tfidf_vect.get_feature_names(), columns=corpus_idx)
    # # print(df)

    # plotData = pandas.DataFrame()
    # plotData['train_data_label_count'] = data_frame.iloc[result[0]].groupby('labels').texts.count()
    # plotData['test_data_label_count'] = test_data_frame.groupby(
    #     'labels').texts.count()
    # fig = plt.figure()
    # plt.title("Data distribution")
    # plotData.plot.bar(ylim=0)
    # plt.show()

    # feature_array = pandas.np.array(tfidf_vect.get_feature_names())
    # tfidf_sorting = pandas.np.argsort(xtrain_tfidf.toarray()).flatten()[::-1]
    # n = 3
    # top_n = feature_array[tfidf_sorting][:n]
    # # print(top_n)
    # encoder = preprocessing.LabelEncoder()
    # train_y = encoder.fit_transform(data_frame['labels'].values.tolist())
    # valid_y = encoder.fit_transform(test_data_frame['labels'].values.tolist())

    nb = MultinomialNB()
    nb.fit(xtrain_tfidf.toarray(), data_frame.iloc[result[0]]['labels'].values)
    # print(test_data_frame['labels'])
    predict_arr = []
    valid_arr = []
    csv_arr = []
    for idx, row in test_data_frame.iterrows():
        print(row['texts'])

        if row['labels'] is 'netral':
            continue

        valid_arr.append(translate_label(row['labels']))
        predicted = nb.predict(tfidf_vect.transform([row['texts']]).toarray())
        print("real value %s, hasil prediksi %s" %
              (row['labels'], predicted[0]))
        # print(predicted)
        predict_arr.append(translate_label(predicted[0]))
        csv_arr.append([row['texts'], row['labels'], predicted[0]])
    # print(test_data_frame['labels'].values)

    # print("akurasi %f" % accuracy_score(predict_arr, valid_arr))
    # print("hamming loss %f" % hamming_loss(predict_arr, valid_arr))
    # print(confusion_matrix(predict_arr, valid_arr))
    # plot_confusion_matrix(predict_arr, valid_arr, ['positif', 'netral', 'negatif'], normalize=False, title='Grafik Confussion Matrix')
    # plt.show()

    plot_confusion_matrix(confusion_matrix(predict_arr, valid_arr), [
                          'Negatif', 'Positif'], normalize=False)

    with open('hasil_prediksi.csv', mode='w') as hasil_prediksi_file:
        pred_writer = csv.writer(
            hasil_prediksi_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        pred_writer.writerow(['text', 'real value', 'hasil prediksi'])

        for row in csv_arr:
            pred_writer.writerow(row)
